<?php

namespace Education\Badges\Controller\Adminhtml\Badge;

use Magento\Backend\App\Action;
use Education\Badges\Model\BadgeFactory;

class NewAction extends Action
{
    protected $collection;

    protected $badgeFactory;

    public function __construct(
        Action\Context $context,
        BadgeFactory $badgeFactory)
    {
        $this->badgeFactory = $badgeFactory;
        parent::__construct($context);
    }

    /**
     * Edit A Badge Page
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */


    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
        $badgeId= $this->getRequest()->getParam('badge_id');

        $badgeData = [
            'badges[id]' => $badgeId,
        ];
        if (is_array($badgeData)) {
            $badge = $this->badgeFactory->create();
            $badge->setData($badgeData);
            $resultRedirect = $this->resultRedirectFactory->create();
        }
    }
}