<?php
    namespace Education\Badges\Controller\Adminhtml\Badge;

    use Magento\Framework\Controller\ResultFactory;
    use Magento\Backend\App\Action\Context;
    use Magento\Ui\Component\MassAction\Filter;
    use Education\Badges\Model\ResourceModel\Badge\CollectionFactory;
    use Magento\Backend\App\Action;

    class Disable extends Action
    {
        /**
         * @var Filter
         */
        protected $filter;

        /**
         * @var CollectionFactory
         */
        protected $collectionFactory;

        /**
         * @param Context $context
         * @param Filter $filter
         * @param CollectionFactory $collectionFactory
         * @param BadgeResource $badgeResource
         */
        public function __construct(
            Context $context,
            Filter $filter,
            CollectionFactory $collectionFactory
        )
        {
            $this->filter = $filter;
            $this->collectionFactory = $collectionFactory;

            parent::__construct($context);
        }
        /**
         * Execute action
         *
         * @return \Magento\Backend\Model\View\Result\Redirect
         * @throws \Magento\Framework\Exception\LocalizedException|\Exception
         */
        public function execute()
        {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $collectionSize = $collection->getSize();
            foreach ($collection as $item) {
                $item->save($item['status'] = '2');
            }


            $this->messageManager->addSuccess(__('A total of %1 record(s) have been disabled.', $collectionSize));

            /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath('*/*/');
        }
    }