<?php
    namespace Education\Badges\Controller\Adminhtml\Badge;

    use Magento\Backend\App\Action;

    class Delete extends Action
    {
        protected $_model;

        /**
         * @param Action\Context $context
         * @param \Education\Badges\Model\Badge $model
         */
        public function __construct(
            Action\Context $context,
            \Education\Badges\Model\Badge $model
        ) {
            parent::__construct($context);
            $this->_model = $model;
        }

        /**
         * {@inheritdoc}
         */
        protected function _isAllowed()
        {
            return $this->_authorization->isAllowed('Education_Badges::badge_delete');
        }

        /**
         * Delete action
         *
         * @return \Magento\Framework\Controller\ResultInterface
         */
        public function execute()
        {
            $badge_id = $this->getRequest()->getParam('badge_id');
            /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            if ($badge_id) {
                try {
                    $model = $this->_model;
                    $model->load($badge_id);
                    $model->delete();
                    $this->messageManager->addSuccess(__('Badge deleted'));
                    return $resultRedirect->setPath('*/*/');
                } catch (\Exception $e) {
                    $this->messageManager->addError($e->getMessage());
                    return $resultRedirect->setPath('*/*/index', ['badge_id' => $badge_id]);
                }
            }
            $this->messageManager->addError(__('Badge does not exist'));
            return $resultRedirect->setPath('*/*/index');
        }
    }