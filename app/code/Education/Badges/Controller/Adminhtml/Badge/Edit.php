<?php
    /**
     * @category    Education
     * @package     Education/Badges
     * @author      Grigorijs Mamilovs <info@scandiweb.com>
     * @copyright   Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
     */

    namespace Education\Badges\Controller\Adminhtml\Badge;

    use Magento\Backend\App\Action;
    use Magento\Backend\App\Action\Context;
    use Magento\Framework\View\Result\PageFactory;
    use Magento\Framework\Registry;
    use Education\Badges\Model\Badge as Badge;


    class Edit extends Action
    {
        protected $_coreRegistry = null;

        protected $resultPageFactory;

        public function __construct(
            Context $context,
            PageFactory $resultPageFactory,
            Registry $registry
        )
        {
            $this->resultPageFactory = $resultPageFactory;
            $this->_coreRegistry = $registry;
            parent::__construct($context);
        }

        protected function _isAllowed()
        {
            return $this->_authorization->isAllowed('Education_Badges::save');
        }

        public function execute()
        {
            var_dump('THIS IS NECESSARY'); die;
            $badge_id = $this->getRequest()->getParam('badge_id');
            $badge = $this->_objectManager->get(Badge::class)->load($badge_id);
            $this->_view->loadLayout();
            $this->_view->renderLayout();
            $badgeData = $this->getRequest()->getParam($badge_id);

            if (is_array($badgeData)) {
                //$badge = $this->_objectManager->create(Badge::class);
                //$badge->setData($badgeData)->save();
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/index');
            }
        }
    }