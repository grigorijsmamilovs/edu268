<?php
namespace Education\Badges\Controller\Adminhtml\Badge;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\Session;
use Magento\Framework\View\Result\PageFactory;
use Education\Badges\Model\Badge;
use Education\Badges\Model\BadgeFactory;
use Education\Badges\Model\ResourceModel\Badge as BadgeResource;

class Save extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Badge
     */
    protected $badgeFactory;
    /**
     * @var Session
     */
    protected $session;
    /**
     * @var BadgeResource
     */
    protected $badgeResource;
    /**
     * @var Filesystem
     */
    protected $_filesystem;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        BadgeFactory $badgeFactory,
        Session $session,
        BadgeResource $badgeResource,
        \Magento\Framework\Filesystem $filesystem
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->badgeFactory = $badgeFactory;
        $this->session = $session;
        $this->badgeResource = $badgeResource;
        $this->_filesystem = $filesystem;
        parent::__construct($context);
    }
    public function execute()
    {
        $badge = $this->getRequest()->getParams();
        $data = $this->getRequest()->getPostValue();
        if (!array_key_exists('badge_id', $badge['badges'])) {

            // Create new badge if ID doesn't exist
            $badgeFactory = $this->badgeFactory->create()->setData([
                'name' => $data['badges']['name'],
                'status' => $data['badges']['status'],
                'comment' => $data['badges']['comment'],
                'path' => $badge['badges']['badge_image'][0]['name']]);

        } else {

            // Edit existing badge with the same ID
            $badgeFactory = $this->badgeFactory->create()->setData([
                'badge_id' => $badge['badges']['badge_id'],
                'name' => $data['badges']['name'],
                'status' => $data['badges']['status'],
                'comment' => $data['badges']['comment'],
                'path' => $badge['badges']['badge_image'][0]['name']]);
            $this->badgeResource->save($badgeFactory);
            $resultRedirect = $this->resultRedirectFactory->create();
        }

        //var_dump($badge['badges']['badge_image'][0]['name']); die;
        $this->badgeResource->save($badgeFactory);
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('education/badge/index');
    }
}