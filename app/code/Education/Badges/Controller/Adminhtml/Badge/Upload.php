<?php

    namespace Education\Badges\Controller\Adminhtml\Badge;

    use Magento\Backend\App\Action;
    use Magento\Backend\App\Action\Context;
    use Magento\Framework\App\Filesystem\DirectoryList;
    use Magento\Framework\App\ResponseInterface;
    use Magento\Framework\Controller\ResultFactory;
    use Magento\Framework\Controller\ResultInterface;
    use Magento\Framework\Exception\FileSystemException;
    use Education\Badges\Model\ImageUploader;

class Upload extends Action
{
    /**
     * @var ImageUploader
     */
    private $imageUploader;
    /**
     *     * @var DirectoryList
     *
     */

    private $dirList;
    /**
     * Upload constructor
     *
     * @param Context $context
     * @param ImageUploader $imageUploader
     * @param DirectoryList $dirList
     */
    public function __construct(
        Context $context,
        ImageUploader $imageUploader,
        DirectoryList $dirList
    )
    {
        parent::__construct($context);
        $this->imageUploader = $imageUploader;
        $this->dirList = $dirList;
    }
    /**
     * Uploading image to directory
     *
     * @return ResponseInterface|ResultInterface
     * @throws FileSystemException
     */
    public function execute()
    {
        $imageId = $this->_request->getParam('param_name', 'image');

        try {
            $result = $this->imageUploader->saveFileToTmpDir($imageId);
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}