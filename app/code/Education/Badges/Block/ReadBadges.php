<?php
    /**
     *  @category    Education
     *  @package     Education\Sales
     *  @author      Grigorijs Mamilovs <info@scandiweb.com>
     *  @copyright   Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
     *  @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
     */

    namespace Education\Badges\Block;

    use Education\Badges\Model\ResourceModel\Badge\CollectionFactory;
    use Magento\Framework\View\Element\Template;
    use Magento\Framework\View\Element\Template\Context;
    use Magento\Store\Model\StoreManagerInterface;
    use Education\Badges\Model\BadgeFactory;
    use Education\Badges\Model\ResourceModel\Badge;

    class ReadBadges extends Template
    {
        protected $collectionFactory;
        /**
         * @var BadgeFactory
         */
        protected $badgeFactory;
        /**
         * @var StoreManagerInterface
         */
        private $storeManagerInterface;

        protected $data;

        /**
         * AddStyleGuidePage constructor.
         *
         * @param Context $context
         * @param BadgeFactory $badgeFactory
         * @param array $data
         * @param StoreManagerInterface $storeManagerInterface
         * @param CollectionFactory $collectionFactory
         */

        public function __construct(
            Context $context,
            BadgeFactory $badgeFactory,
            array $data = [],
            StoreManagerInterface $storeManagerInterface,
            CollectionFactory $collectionFactory
        )

        {
            $this->collection = $badgeFactory->create();
            $this->storeManagerInterface = $storeManagerInterface;
            $this->collectionFactory = $collectionFactory;
            parent::__construct($context);
        }

        /**
         * @return \Education\Badges\Model\ResourceModel\Badge\CollectionFactory
         */

        public function getBadges()
        {
            return $this->badgeFactory->create()
                ->addFieldToSelect()
                ->addFieldToFilter()
                ->addFieldToFilter()
                ->setOrder();
        }

        public function getCollection()
        {
            return $this->collectionFactory->create();
        }
    }