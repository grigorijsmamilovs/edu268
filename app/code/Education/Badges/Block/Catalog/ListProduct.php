<?php
    /**
     * @category    Education
     * @package     Education\Badges
     * @author      Grigorijs Mamilovs <info@scandiweb.com>
     * @copyright   Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
     * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
     */

    namespace Education\Badges\Block\Catalog;

    use http\Env\Request;
    use Magento\Catalog\Api\CategoryRepositoryInterface;
    use Magento\Catalog\Block\Product\Context;
    use Magento\Catalog\Block\Product\ListProduct as BaseListProduct;
    use Magento\Catalog\Model\Layer\Resolver;
    use Magento\Framework\Data\Helper\PostHelper;
    use Magento\Framework\Url\Helper\Data;
    use Magento\Store\Model\StoreManagerInterface;
    use Education\Badges\Model\ResourceModel\Badge\CollectionFactory;

    /**
     * Class ListProduct
     * @package Scandiweb\BadgesPage\Block\Catalog
     */
    class ListProduct extends BaseListProduct
    {
        protected $collectionFactory;

        protected $storeManager;
        /**
         * ListProduct constructor.
         * @param Context $context
         * @param PostHelper $postDataHelper
         * @param Resolver $layerResolver
         * @param CategoryRepositoryInterface $categoryRepository
         * @param Data $urlHelper
         * @param CollectionFactory $badgeCollection
         * @param StoreManagerInterface $storeManager
         * @param array $data
         */
        public function __construct(
            Context $context,
            PostHelper $postDataHelper,
            Resolver $layerResolver,
            CategoryRepositoryInterface $categoryRepository,
            Data $urlHelper,
            CollectionFactory $badgeCollection,
            StoreManagerInterface $storeManager,
            array $data = [])
        {
            $this->collectionFactory = $badgeCollection;
            $this->storeManager = $storeManager;
            parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $data);
        }

        /**
         * @param $badge_id
         * @return mixed
         */
        public function getBadgeStatus($badge_id)
        {
            $badgeStatus = $this->collectionFactory->create()->getItemById($badge_id);
            if (isset($badgeStatus))
                return $this->collectionFactory->create()->getItemById($badge_id)->getbadge_status();
        }

        /**
         * @param $id
         * @return string
         * @throws \Magento\Framework\Exception\NoSuchEntityException
         */
        public function getBadgeUrl($badge_id) // not given proper argument
        {
            $data = $this->collectionFactory->create()->getItemById($badge_id)->getData();

            return $this->storeManager->getStore()->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                ) . 'badge/' . $data['path']; // not a propper link
        }
    }
