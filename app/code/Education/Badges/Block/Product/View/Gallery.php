<?php
    /**
     * @category    Education
     * @package     Education/Badges
     * @author      Grigorijs Mamilovs <info@scandiweb.com>
     * @copyright   Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
     * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
     */

    namespace Education\Badges\Block\Product\View;

    use Magento\Catalog\Block\Product\Context;
    use Magento\Catalog\Block\Product\View\Gallery as BaseGallery;
    use Magento\Catalog\Model\Product\Gallery\ImagesConfigFactoryInterface;
    use Magento\Catalog\Model\Product\Image\UrlBuilder;
    use Magento\Framework\Json\EncoderInterface;
    use Magento\Framework\Stdlib\ArrayUtils;
    use Magento\Store\Model\StoreManagerInterface;
    use Education\Badges\Model\ResourceModel\Badge\CollectionFactory;

    class Gallery extends BaseGallery
    {
        protected $collectionFactory;

        protected $storeManager;

        public function __construct(
            Context $context,
            ArrayUtils $arrayUtils,
            EncoderInterface $jsonEncoder,
            CollectionFactory $collectionFactory,
            StoreManagerInterface $storeManager,
            ImagesConfigFactoryInterface $imagesConfigFactory = null,
            array $galleryImagesConfig = [],
            UrlBuilder $urlBuilder = null,
            array $data = []
        ) {

            $this->collectionFactory = $collectionFactory;
            $this->storeManager = $storeManager;
            parent::__construct($context, $arrayUtils, $jsonEncoder, $data, $imagesConfigFactory, $galleryImagesConfig,
                $urlBuilder);
        }

        public function getMediaUrl()
        {
            return $this->storeManager->getStore()->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                ) . 'badge/';

        }

        public function getBadgeStatus($badge_id)
        {
            $badgeStatus = $this->collectionFactory->create()->getItemById($badge_id);
            if (isset($badgeStatus)) {
                return $this->collectionFactory->create()->getItemById($badge_id)->getbadge_status();
            }
        }

        public function getBadgeUrl($badge_id)
        {
            $data = $this->collectionFactory->create()->getItemById($badge_id)->getData();

            return $this->storeManager->getStore()->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                ) . 'badge/' . $data['path'];
        }
    }