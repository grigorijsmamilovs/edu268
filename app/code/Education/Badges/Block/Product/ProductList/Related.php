<?php
    /**
     * @category    Education
     * @package     Education/Badges
     * @author      Grigorijs Mamilovs <info@scandiweb.com>
     * @copyright   Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
     * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
     */

namespace Education\Badges\Block\Product\ProductList;

use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Block\Product\ProductList\Related as BaseRelated;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Checkout\Model\ResourceModel\Cart;
use Magento\Checkout\Model\Session;
use Magento\Framework\Module\Manager;
use Magento\Store\Model\StoreManagerInterface;
use Education\Badges\Model\ResourceModel\Badge\CollectionFactory;

/**
 * Class Related
 * @package Scandiweb\BadgesPage\Block\Product\ProductList
 */
class Related extends BaseRelated
{
    protected $collectionFactory;

    protected $storeManager;

    public function __construct(
        StoreManagerInterface $storeManager,
        CollectionFactory $collectionFactory,
        Context $context,
        Cart $checkoutCart,
        Visibility $catalogProductVisibility,
        Session $checkoutSession,
        Manager $moduleManager,
        array $data = []
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->storeManager = $storeManager;
        parent::__construct($context, $checkoutCart, $catalogProductVisibility, $checkoutSession, $moduleManager, $data);
    }
    /**
     * @param $id
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBadgeUrl($badge_id)
    {
        $data = $this->collectionFactory->create()->getItemById($badge_id)->getData();

        return $this->storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            ) . 'badge/' . $data['path'];
    }
    /**
     * @param $badge_id
     * @return mixed
     */
    public function getBadgeStatus ($badge_id){
        $badgeStatus = $this->collectionFactory->create()->getItemById($badge_id);
        if (isset($badgeStatus))
            return $this->collectionFactory->create()->getItemById($badge_id)->getbadge_status();
    }
}
