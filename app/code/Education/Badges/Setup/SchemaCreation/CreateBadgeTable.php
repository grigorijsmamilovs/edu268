<?php
/**
 * @category    Education
 * @package     Education\Badges
 * @author      Grigorijs Mamilovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
 */
namespace Education\Badges\Setup\SchemaCreation;

use Scandiweb\Migration\Api\MigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Framework\DB\Ddl\Table;


class CreateBadgeTable implements MigrationInterface
{
    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {

        $installer = $setup;

        $installer->startSetup();

        if (!$installer->tableExists('badges')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('badges'))
                ->addColumn(
                    'badge_id',
                    Table::TYPE_INTEGER,
                    10,
                    ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true]
                )
                ->addColumn(
                    'badge_name',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false])
                ->addColumn(
                    'status',
                    Table::TYPE_TEXT,
                    10,
                    ['nullable' => true])
                ->addColumn(
                    'image_path',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Image path')
                ->addColumn(
                    'store_id',
                    10,
                    ['default' => 0])
                ->addColumn(
                    'badge_comment',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true])
                ->setComment(
                    'Badges Table'
                );
            var_dump($table); die;

            $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }
}
