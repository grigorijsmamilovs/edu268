<?php
/**
 * @category  Education
 * @package   Education\Badges
 * @author    Grigorijs Mamilovs <info@scandiweb.com>
 * @copyright Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Education\Badges\Setup;

use Scandiweb\Migration\Setup\AbstractUpgradeSchema;

use Education\Badges\Setup\SchemaCreation\CreateBadgeTable;

class UpgradeSchema extends AbstractUpgradeSchema
{
    protected $migrations = [
        '0.0.1' => CreateBadgeTable::class,
    ];
}
