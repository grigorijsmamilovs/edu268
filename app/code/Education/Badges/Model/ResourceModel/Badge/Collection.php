<?php
/**
 * @category    Education
 * @package     Badges\Sales
 * @author      Grigorijs Mamilovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Education\Badges\Model\ResourceModel\Badge;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'badge_id';
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Education\Badges\Model\Badge',
            'Education\Badges\Model\ResourceModel\Badge'
        );
    }
}