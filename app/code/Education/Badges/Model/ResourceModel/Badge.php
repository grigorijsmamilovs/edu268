<?php

/**
 * @category    Education
 * @package     Education\Sales
 * @author      Grigorijs Mamilovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Education\Badges\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Badge extends AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('badges', 'badge_id');
    }
}
