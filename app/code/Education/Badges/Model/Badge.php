<?php
/**
 * @category    Education
 * @package     Education\Sales
 * @author      Grigorijs Mamilovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Education\Badges\Model;

use Magento\Framework\Model\AbstractModel;

class Badge extends AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'badges';
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Education\Badges\Model\ResourceModel\Badge');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
