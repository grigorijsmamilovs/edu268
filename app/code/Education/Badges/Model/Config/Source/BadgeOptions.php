<?php
namespace Education\Badges\Model\Config\Source;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Education\Badges\Model\ResourceModel\Badge\CollectionFactory;

class BadgeOptions extends AbstractSource
{
    public function __construct(
        CollectionFactory $badgeCollectionFactory
    ){
        $this->collection = $badgeCollectionFactory->create();
    }
    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $items = $this->collection->getItems();
        $i = 0;
        foreach ($items as $badge) {
            $this->options[$i] = [
                'label' => $badge['name'],
                'value' => $badge['badge_id']
            ];
            $i++;
        }
        return $this->options;
    }
}