<?php

namespace Education\Badges\Model\Badge;

use Education\Badges\Model\ResourceModel\Badge\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Request\DataPersistorInterface;


class DataProvider extends AbstractDataProvider
{
    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $contactCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $badgeCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $badgeCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);

    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }


        $items = $this->collection->getItems();
        $this->loadedData = array();
        /** @var Badge $badge */
        foreach ($items as $badge) {
            $this->loadedData[$badge->getId()]['badges'] = $badge->getData();
        }
        return $this->loadedData;
    }
}